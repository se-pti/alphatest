import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import NewContact from './components/NewContact';
import AllContacts from "./components/AllContacts";

import './custom.css'

export default () => (
    <Layout>
        <Route exact path='/' component={NewContact} />
        <Route exact path='/all-contacts' component={AllContacts} />
    </Layout>
);
