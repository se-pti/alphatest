import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApplicationState } from '../store';
import * as ContactsStateStore from "../store/Contact";


type ContactsProps =
    ContactsStateStore.Contact
    & typeof ContactsStateStore.actionCreators
    & RouteComponentProps<{}>; 

class Contact extends React.PureComponent<ContactsProps> {
    public render() {
        return (
            <React.Fragment>
                <h1>Создать контакт</h1>
                <div className="row my-2">
                    <label className="col-2">ФИО</label>
                    <input className="col-4" type="text" id="fullName"/>
                </div>
                <div className="row my-2">
                    <label className="col-2">Номер телефона</label>
                    <input className="col-4" type="text" id="phoneNumber"/>
                </div>
                <div className="row my-2">
                    <label className="col-2">Электронная почта</label>
                    <input className="col-4" type="text" id="email"/>
                </div>

                <Link className='btn btn-primary btn-lg my-2'
                    onClick={(e) => {
                        let fullName = document.getElementById("fullName").value;
                        let phoneNumber = document.getElementById("phoneNumber").value;
                        let email = document.getElementById("email").value;
                        this.props.insertContact(fullName, phoneNumber, email);
                    }}
                    to={`/all-contacts`}>
                    Сохранить
                    </Link>
            </React.Fragment>
        );
    }
}


export default connect(
    (state: ApplicationState) => state.contacts, 
    ContactsStateStore.actionCreators
)(Contact as any);
