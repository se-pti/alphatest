import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store';
import * as ContactsStateStore from "../store/Contact";


type ContactsProps =
    ContactsStateStore.ContactsState
    & typeof ContactsStateStore.actionCreators
    & RouteComponentProps<{}>;

class Contacts extends React.PureComponent<ContactsProps> {

    public render() {
        return (
            <React.Fragment>
                <h1 id="tabelLabel">Все контакты</h1>
                {this.renderContactsTable()}
            </React.Fragment>
        );
    }

    private renderContactsTable() {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                <tr>
                    <th>ФИО</th>
                    <th>Номер телефона</th>
                    <th>E-mail</th>
                </tr>
                </thead>
                <tbody>
                    {this.props.contacts.map((contact: ContactsStateStore.Contact) =>
                        <tr key={contact.phone}>
                            <td>{contact.fullName}</td>
                            <td>{contact.phone}</td>
                            <td>{contact.email}</td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }
}


export default connect(
    (state: ApplicationState) => state.contacts,
    ContactsStateStore.actionCreators
)(Contacts as any);
