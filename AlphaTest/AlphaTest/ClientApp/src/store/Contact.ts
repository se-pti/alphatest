import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';


// STATE
export interface ContactsState {
    contacts: Contact[];
}

export interface Contact {
    fullName: string;
    phone: string;
    email: string;
}


// ACTIONS
interface InsertContactAction {
    type: 'INSERT_CONTACT';
    contact: Contact;
}


// ACTION CREATORS 
export const actionCreators = {
    insertContact: (fullName: string, phone: string, email: string): AppThunkAction<InsertContactAction> => (dispatch, getState) => {
        const cont: Contact = { fullName: fullName, phone: phone, email: email };
        dispatch({ type: 'INSERT_CONTACT', contact: cont} );
    }
};


// REDUCER 
const unloadedState: ContactsState = { contacts: []};

export const reducer: Reducer<ContactsState> = (state: ContactsState | undefined, incomingAction: Action): ContactsState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as InsertContactAction;
    switch (action.type) {
        case 'INSERT_CONTACT':
            return {
                contacts: state.contacts.concat((action as InsertContactAction).contact) 
            };
    }

    return state;
};
